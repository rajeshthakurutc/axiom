import json
import logging.config
import logging.handlers
import os
import sys
import time
import traceback
import uuid
from importlib import import_module

import rabbitpy
import rabbitpy.exceptions
from putils.Clients import RPCConsumer
from putils.Interfaces.CentralDBInterface import CentralDBInterface
from putils.config.LoggingAdapter import ContextFilter
from putils.helpers import argument_parsing

from axiom.runner import run
from version import __version__


class WorkerConsumer(RPCConsumer):
    """
    Worker consumer subclasses RPCConsumer
    """

    def __init__(self, username, password, hostname, rpc_queue_name, name, cdb, force_queue=False, **kwargs):
        log_path = str(os.path.dirname(os.path.abspath(__file__))) + os.sep + 'runtime'
        super(WorkerConsumer, self).__init__(username, password, hostname, rpc_queue_name, name, log_path, cdb,
                                             force_queue=force_queue, **kwargs)

    @staticmethod
    def callback(message, name):
        run(message, name, vault_url=config.VAULT_URL, vault_role=config.VAULT_ROLE)


def cleanup_logs():
    """
    Deletes any logs more than a week old

    :return:
    """

    days_to_keep = 7

    path = os.getcwd() + os.sep + 'runtime'
    now = time.time()
    for f in os.listdir(path):
        if os.stat(os.path.join(path, f)).st_mtime < now - (days_to_keep * 3600 * 24) and 'runtime' in f:
            if os.path.isfile(f):
                os.remove(os.path.join(path, f))

    return True


def check_runtime_log(username, password, hostname, log, worker_name=None):
    """
    Goes into the runtime directory, and tries to find a runtime file. If one exists and has a task in it, then an error
    is replied on the appropriate queue, otherwise we just make one and continue on. The string-ed worker name is returned

    The format for one of these is json:

    {
        "in_progress": {
            "task_body": string,
            "task_reply_correlation_id": string,
            "task_reply_routing_key": string
        }
    }

    :param worker_name:
    :return:
    """

    if worker_name is None:
        worker_name = str(uuid.uuid4())

    filename = os.getcwd() + os.sep + 'runtime' + os.sep + str(worker_name) + '.runtime'

    if os.path.isfile(filename):
        # grab the in progress task
        try:
            inprog = json.load(open(filename, 'r'))

            # free the executor
            if 'in_progress' in inprog.keys():
                # reply with just a generic error
                response = 'ERROR: worker found in-progress task log on startup:\n %s' % inprog['in_progress'][
                    'task_body']
                connection_string = 'amqp://%s:%s@%s:5672' % (username, password, hostname)
                with rabbitpy.Connection(connection_string) as connection:
                    with connection.channel() as channel:
                        corr_id = inprog['in_progress']['task_reply_correlation_id']
                        routing_key = inprog['in_progress']['task_reply_routing_key']
                        reply = rabbitpy.Message(channel, response, properties={'correlation_id': corr_id})
                        reply.publish('', routing_key=routing_key, mandatory=True)
        except Exception as e:
            log.error(e)

        # clear the file out
        json.dump({}, open(filename, 'w'))
    else:
        json.dump({}, open(filename, 'w'))

    cleanup_logs()

    return worker_name


def log_exceptions(exctype, value, tb):
    """
    Intercepts uncaught exception messages and logs them before anything messes up.

    :param exctype:
    :param value:
    :param tb:
    :return:

    """

    LOG.error('TYPE: %s', (exctype.args,))
    LOG.error('VALUE: %s', (value,))
    LOG.error('TRACEBACK: %s', (''.join(traceback.format_exception(exctype, value, tb, 10)),))


def load_logging_config(file_name):
    logging.config.dictConfig(
        json.load(open(
            project_root + os.sep + 'axiom' + os.sep + 'config' + os.sep + file_name,
            'r')))


if __name__ == '__main__':
    # parse out sys args if there are any. Here we set the environment that the worker is running in, so it can fetch
    # default values off disk to connect to cdb, we also let it know where it is and it's name. If these aren't passed
    # they are generated or inferred based on the cloud provider's API.

    ARGS, ENV, NAME, EUROPE, FORCEQUEUE, __ = argument_parsing.parse_arguments(sys.argv)

    NAME = NAME or RPCConsumer.get_worker_name('LARRY_WORKER')

    # Dynamically figure out the project root for importing the logging configs.
    project_root = str(os.path.dirname(os.path.abspath(__file__)))
    os.chdir(project_root)

    member_type = 'EU_AXIOM_WORKER' if EUROPE else 'AXIOM_WORKER'

    ENV = os.getenv('ENV', 'LOCAL').upper() if not ENV else ENV
    # Import the env settings and the logging configs, defaults to DEV if the environment is not recognized.
    if ENV.upper() in ['DEV', 'TEST', 'PROD']:
        config = import_module('axiom.config.kube_settings')
        load_logging_config('logging.{env}.json'.format(env=ENV.lower()))
    else:
        config = import_module('axiom.config.local_dev_settings')
        load_logging_config('logging.dev.json')

    # Set up the logger and add in the filter to log out stack traces. This is for local on-disk logs, not for the
    # centralized logs handled by loggy mclogface.
    LOG = logging.getLogger('WORKER')
    f = ContextFilter()
    LOG.addFilter(f)
    sys.excepthook = log_exceptions

    LOG.info("Loaded settings for {} environment".format(ENV))

    # Note: if we add more regions (like azure) we should then generalize this better.

    listening_queue = config.EUROPE_AXIOM_QUEUE if EUROPE else config.AXIOM_QUEUE

    # setup a central db connection
    cdb = CentralDBInterface(config.CONFIG_HOSTNAME, config.CONFIG_USERNAME, config.CONFIG_PASSWORD,
                             vault_url=config.VAULT_URL, vault_role=config.VAULT_ROLE)

    # resolve the queue hostname
    # res = cdb.driver.execute('SELECT queue_private_ip from nike.queues limit 1', fetch=True)
    # if len(res) > 0:
    #     queue_hostname = res[0][0]
    # else:
    #     # if a hostname cant be found for whatever reason, fall back to our on-disk default.
    #     queue_hostname = QUEUE_HOSTNAME

    # Instantiate a consumer, run it, and clean it up/restart on error
    while True:
        consumer = None

        # first we add ourselves to the members table in nike (CDB), before we try to start up.
        cdb.add_member(NAME, member_type, version=__version__)

        # our name is passed to runtime log to see if we are starting up after some kind of failure, if so, the
        # failure is appropriately handled before listening on the queue.
        nm = check_runtime_log(config.QUEUE_USERNAME, config.QUEUE_PASSWORD, config.QUEUE_HOSTNAME, LOG, NAME)
        try:
            # consumer is an instance of RPCConsumer, with callback specified to run the axiom services.
            consumer = WorkerConsumer(
                config.QUEUE_USERNAME,
                config.QUEUE_PASSWORD,
                config.QUEUE_HOSTNAME,
                listening_queue,
                name=nm,
                cdb=cdb,
                force_queue=FORCEQUEUE,
                member_type=member_type
            )

            # run will just sit in a while loop, popping off messages and processing them as long as possible.
            consumer.run()
        except KeyboardInterrupt as kbi:
            # if the user kills the worker, we want to clean up CDB state before exiting.
            cdb.delete_member(NAME)
            del consumer
            break
        except Exception as err:
            # on exceptions we print out full tracebacks and clean up CDB state.
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_tb(exc_tb)
            cdb.delete_member(NAME)
            del consumer
            LOG.warning('RESTARTING WORKER')
