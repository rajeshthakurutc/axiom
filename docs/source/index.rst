Axiom
=====

author: Marlee Stevenson, Will McGinnis

An importable library and runtime containing the axiom worker.  Axiom is broken into two
primary sections:

 * runtime nike worker
 * importable library (lib-snipe)

This documentation focuses on the 2nd section, and how to use axiom as an importable library.

Functional Overview
-------------------

What follows is a high level functional overview of the objects that make up the axiom library.


Evaluations
^^^^^^^^^^^

This package contains functions for getting signals, evaluation, thresholding, and applying rules.

Plots
^^^^^^^

This package has the ability to create a regular plot or an underlying plot.

Processes
^^^^^^^^^

This package runs the end to end processing which includes thresholding, plotting, evaluating, and watchlist creation.

Watchlists
^^^^^^^^^^

This package has the ability to create a regular watchlist or a blended watchlist.

Developers' Reference
---------------------


Documentation
^^^^^^^^^^^^^

You're looking at the documentation.  The docs from the most recent release are hosted on PCC, accessible from the home
page. When you add new transformers, wrappers, or anything, please update and write docstrings and add them to the docs
directory.  Currently, much of the documentation is lacking, this is a work in progress and any help is appreciated.

Releases
^^^^^^^^

Releases are frequent and will be announced on the #datascience slack channel. Release notes are in changelog.md.

Issues
^^^^^^

If there are any issues, bugs or feature requests, please create an issue on JIRA or talk to Will/Dev/Marlee.

Pull Requests / Branching
^^^^^^^^^^^^^^^^^^^^^^^^^

To contribute to this repo, please create a feature branch off of develop (git-flow), push your changes to that feature
branch, and submit a pull request against develop.

Usage
^^^^^

To install Axiom, be on the VPN and run:

.. code-block:: shell

   /path/to/pip install -U -i http://10.0.0.133:6543/pypi/ --trusted-host 10.0.0.133 axiom==x.x.x

Where x.x.x is the version you'd like to install (or just "axiom" for latest)

Contents:
---------
.. toctree::
   :maxdepth: 2

   runner
   service_directory