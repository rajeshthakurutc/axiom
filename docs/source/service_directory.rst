=================
Service Directory
=================


-----------

Batchend2End
------------

Description
~~~~~~~~~~~

Batch End2End

Configuration Options
~~~~~~~~~~~~~~~~~~~~~

- **good_cohort**: (str) file path of good cohort json
- **bad_cohort**: (str) file path of bad cohort json
- **unknown_cohort**: (str) file path of unknown cohort json
- **bucket_name**: (str) s3 bucket to read from/write to
- **access_key**: (str) s3 access key
- **secret_key**: (str, optional deafult=None) s3 secret key
- **file**: (str) full filepath/filename(s) of csv data (all filepath/filenames in one string separated by commas)
- **save_dir**: (str) full filepath for output directory
- **devicecol**: (str, optional deafult=device_id) column name that contains device id
- **datecol**: (str, optional deafult=time_stamp) column name that contains date
- **timeunit**: (str, optional deafult=ns) time unit
- **rulefile**: (str) a valid rulefile containing all rules to be executed in this batch
- **iters**: (int, optional deafult=10) number of iterations in the threshold optimization search
- **favor_recall**: (bool, optional deafult=False) if true, will prefer high recall models over high precision

Example
~~~~~~~

.. code-block:: javascript

    {}


.. autoclass:: axiom.services.batch_end2end.BatchEnd2End
        :members:

-----------

Blendedwatchlist
----------------

Description
~~~~~~~~~~~

Blended Watchlist

Configuration Options
~~~~~~~~~~~~~~~~~~~~~

- **rule_file**: (str) file path of rule json
- **scaling_factors**: (str) full filepath/filename of scaling factors json
- **bucket_name**: (str) s3 bucket to read from/write to
- **access_key**: (str) s3 access key
- **secret_key**: (str, optional deafult=None) s3 secret key
- **file**: (str) full filepath/filename(s) of csv data (all filepath/filenames in one string separated by commas)
- **save_dir**: (str) full filepath for output directory
- **devicecol**: (str, optional deafult=device_id) column name that contains device id
- **datecol**: (str, optional deafult=time_stamp) column name that contains date
- **timeunit**: (str, optional deafult=ns) time unit
- **date**: (str) date string in the format of i.e. 2018-03-02
- **plot_top_n**: (int, optional deafult=0) plot the top n results

Example
~~~~~~~

.. code-block:: javascript

    {}


.. autoclass:: axiom.services.blended_watchlist.BlendedWatchlist
        :members:
