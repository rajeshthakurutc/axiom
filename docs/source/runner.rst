Runner
=======

Runner
------

The module for running axiom/lib-snipe.

AxiomRunner
^^^^^^^^^^^^^

Pulls data and configs, runs rules and provides an output with confusion matrix and rules ranking.
