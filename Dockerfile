FROM python:3.5.2
ENV PYTHONUNBUFFERED 1

RUN set -x \
    && mkdir -p /usr/predikto \
    && chmod -R 755 /usr/predikto \
    && addgroup --gid 1000 jenkins \
    && useradd -u 1000 -g 1000 --home /home/jenkins --shell /bin/bash jenkins --no-create-home \
    && usermod -aG jenkins root && mkdir -p /home/jenkins \
    && chown jenkins:jenkins -R /home/jenkins \
    && chown jenkins:jenkins -R /usr/predikto

ADD .pypirc /home/jenkins
RUN pip install virtualenv

USER jenkins
WORKDIR /home/jenkins
RUN virtualenv venv
ADD requirements.txt .
RUN MAKEFLAGS="-j$(nproc)" ~/venv/bin/pip install --upgrade --index-url=http://10.0.0.133:6543/pypi/ --trusted-host=10.0.0.133 pipdeptree \
 && MAKEFLAGS="-j$(nproc)" ~/venv/bin/pip install --upgrade --index-url=http://10.0.0.133:6543/pypi/ --trusted-host=10.0.0.133 -r requirements.txt
