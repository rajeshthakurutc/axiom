#!/bin/bash

set -eu -o pipefail

function main {
  #
  # Generate a unique name. hostname is the pod's
  # name and is only unique within a k8s cluster.
  #
  declare -ga PID_LIST=()

  trap "cleanup 'EXIT' '$$'" EXIT
  trap "cleanup  'INT' '$$'"  INT
  trap "cleanup 'TERM' '$$'" TERM

  local worker_dir='/usr/predikto/container/axiom'
  local default_name default_name_arg name_arg=

  default_name="axiom_worker_container_$(hostname -s)_$(uuidgen)"
  default_name_arg="NAME=$default_name"
  name_arg="NAME=$default_name"

  for arg; do
    case "$arg" in
    'NAME='?*) name_arg="$arg";;
    esac
  done

  #---------------------------------------------------------------------------

  source /usr/predikto/container/common/scripts/functions.sh \
    && export_k8s_token_value \
    || :

  "${worker_dir}/venv352/bin/python" "${worker_dir}/worker.py" \
    "$default_name_arg" \
    "$@"
}

function cleanup {
    declare signal=$1; shift
    declare pid=$1; shift
    declare i

    log_msg INFO "Got signal $signal: Currernt pid is $pid"

    # Cleanup in reverse order
    for ((i=${#PID_LIST[@]}-1; i>=0; i--)); do
      log_msg INFO "Terminating pid $i"
      kill -TERM "${PID_LIST[i]}" || :
    done

    PID_LIST=()
}

function log_msg {
  declare level=$1; shift

  printf "%s: %5s %s\n" "$(date +'%Y-%m-%d %H:%M:%S%z')" "$level" "$@"
}

main "$@"
