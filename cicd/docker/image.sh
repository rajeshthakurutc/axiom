#!/usr/bin/env bash
#BEGIN:
#
# usage: image.sh <command>
#   builds a docker image
#
#
# The default registry is:
#     425470256342.dkr.ecr.us-east-1.amazonaws.com
#
# To tag/push the docker image to a different registry, assign the new registry
# to the REGISTRY env variable.
#
# To tag the docker image to your local docker, set the env var REGISTRY
# to the empty string.
#
# image.sh [command] [options] [-- [docker-build-options | docker-run-options]]
#
#   Options:
#     -e <dev | test | prod>  Set the environment: Defaults to 'test'
#
#     -r <region>             Set the region: Defaults to 'us-east-1'
#                             The region is used in the docker registry name:
#                             425470256342.dkr.ecr.<region>.amazonaws.com
#
#     -n <release-id>       This value is appended to the end of the docker
#                           tag: eg. axiom-prod-3.0.6-<release-id>
#                           Defaults to 1
#
#     -x                    Don't pull the code
#                           The default is to pull the code from s3 or git
#
#     -b <branch>           The git branch to use to build the contianer
#                           Clone the repo and checkout the given branch.
#
#     -d <git-working-dir>  Use an existing git dir
#                           This is useful for testing uncommitted git
#                           changes. The '-b <branch>' option is ignored when
#                           this option is given.
#
#                           Uncommitted and indexed changes are included
#                           when this options is used.
#
#                           When using this option, include the options
#                           '-- --no-cache' at the end of the 'image.sh build'
#                           command if there are changes in that repo that
#                           are not cached in local docker images.
#
# image.sh build [options] [-- [docker-build-options]]
#
#     [docker-build-options]  Any valid 'docker build' options.
#
#   The docker image name is axiom-<env>.
#   The worker's name defaults to:
#     "axiom_container_worker_$(hostname -s)_$(uuidgen)"
#
#   Start the container with:
#     docker run axiom-<env> NAME=<new-worker-name>
#
#   Start the container with:
#     docker run --rm --restart=on-failure axiom-<env> NAME=<unique-worker-name>
#
# image.sh create_repo [options]
#     Creates the aws repo by running:
#       aws ecr create-repository --repository-name "$NAME"
#
# image.sh push [options]
#   pushes the image
#
# image.sh show [options]
#   shows the image's name
#
# image.sh run [options] [-- [docker-run-args]]
#   executes 'docker run --detach=true [docker-run-args] <image-name>'
#
# image.sh run_bash [options] [-- [docker-run-args]]
#   executes 'docker run -it [docker-run-args] <image-name> /bin/bash
#
#END:
#
set -eu -o pipefail

readlink=$(type -p greadlink || type -p readlink)

function usage {
  sed -e '0,/^#\s*BEGIN:$/d; /^#\s*END:\s*$/,$d; s/^#$//g; s/^#\s/ /g' < $0
}

function aws_usage {
  cat 1>&2 <<EOF
Env vars AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY are needed to build
this docker image.

EOF
}

function main {
  declare -lg ENV_NAME=test
  declare -ug ENV="$ENV_NAME"
  declare -lg REGION='us-east-1'
  declare -ga OTHER_ARGS=()
  declare -g RELEASE=1
  declare -g BUILD_DIR DEBUG_IMAGE IMAGE REGISTRY SOURCE VERSION VERSION_FILE
  declare docker_file_content

  export DONT_PULL= SCRIPT_DIR= REGISTRY_SLASH= TIMESTAMP=

  TIMESTAMP=$(date -u '+%Y-%m-%d %H:%M:%S %z')
  SCRIPT_DIR=$(dirname "$("$readlink" -f "$0")")

  case "${1:-}" in
  create_dockerfile) "$@";;
  version) shift; create_dockerfile "$@" \
    | sed -nre \
      "s/^LABEL\\s+(.*)\$/\\1/;s/.*\\s+VERSION=[\"']([^\"']+)[\"'].*/\\1/p"
    ;;

  help|-h|--help) usage;;
  '') usage;;
  *)
    command="$1"; shift

    # Detect invalid commands early
    #
    # This is a duplicate of the case statement below with noops for valid
    # commands.
    case "$command" in
    show) ;;
    show_debug) ;;
    build|create_repo|run|run_debug|dockerfile_debug|build_debug|push) ;;
    *) echo "Unknown command: '${command}'"$'\n'"$(usage)" >&2; exit 1;;
    esac

    if [ -s "$SCRIPT_DIR/Dockerfile" ]; then
      docker_file_content=$(< "$SCRIPT_DIR/Dockerfile")

      create_dockerfile "$@" \
        > "$SCRIPT_DIR/Dockerfile" <<<"$docker_file_content"
    else
      create_dockerfile "$@" \
        > "$SCRIPT_DIR/Dockerfile" < /dev/null
    fi

    if [ ! -s "$SCRIPT_DIR/Dockerfile" ]; then
      cat <<EOF >&2
The '$command' needs the Dockerfile.  Create the Dockerfile with:

$0 show <args>
EOF
      exit 1
    fi

    if [ ${#OTHER_ARGS[@]} -gt 0 ]; then
      set -- "${OTHER_ARGS[@]}"
    else
      set --
    fi

    get_values_from_dockerfile

    IMAGE="${REGISTRY:+${REGISTRY}/}${NAME}:${VERSION}${RELEASE:+-${RELEASE}}"
    DEBUG_IMAGE="${REGISTRY:+${REGISTRY}/}${NAME}-debug:${VERSION}${RELEASE:+-${RELEASE}}"

    case "$command" in
    show) echo $IMAGE;;
    show_debug) echo $DEBUG_IMAGE;;
    build|create_repo|run|run_debug|dockerfile_debug|build_debug|push) "$command" "$@";;
    *) echo "Unknown command: '${command}'"$'\n'"$(usage)" >&2; exit 1;;
    esac
  esac
}

function create_dockerfile {
  exec 3>&1 1>&2 # save stdout to fd 3 and forward stdout to stderr

  declare branch=
  declare git_working_dir=
  declare docker_file_content

  while getopts 'e:r:b:d:n:x' opt; do
    case "$opt" in
    b) branch="$OPTARG";;
    d) git_working_dir="$OPTARG";;
    n) RELEASE="$OPTARG";;
    r) REGION=$OPTARG;;
    x) DONT_PULL=1;;
    e)
      shopt -s nocasematch
      trap 'shopt -u nocasematch' 0 1 2 13 15
      case "$OPTARG" in
      test|TEST|dev|DEV|prod|PROD|local|LOCAL) ENV_NAME=$OPTARG; ENV=$OPTARG;;
      *)
        cat <<EOF
Unknown environment '$OPTARG'

$(usage)
EOF
        exit 1
        ;;
      esac
      shopt -u nocasematch
      trap - 0 1 2 13 15
      ;;
    ?) usage >&2; exit 1;;
    esac
  done

  shift $((OPTIND - 1)) || :

  if [ $# -gt 0 ]; then
    OTHER_ARGS=("$@")
  fi

  if [ -z ${REGISTRY+x} ]; then
    REGISTRY="425470256342.dkr.ecr.${REGION}.amazonaws.com"
    REGISTRY_SLASH="${REGISTRY}/"
  elif [ -n "${REGISTRY}" ]; then
    REGISTRY_SLASH="${REGISTRY}/"
  fi

  BUILD_DIR="${SCRIPT_DIR}/./src/${ENV_NAME}"
  VERSION_FILE="$BUILD_DIR/version.py"

  if [ -z "$DONT_PULL" ]; then
    /bin/rm -rf "$BUILD_DIR"
    mkdir -vp "$BUILD_DIR"

    if [ -z "$branch" -a -z "$git_working_dir" ]; then
      pull_code_from_s3
    else
      pull_code_from_git "$branch" "$git_working_dir"
    fi
  fi

  get_version "$VERSION_FILE"

  echo "Preparing a docker file for axiom version ${VERSION}-${RELEASE} ENV=$ENV_NAME in region $REGION"
  sleep 2

  if [ -z "$DONT_PULL" ]; then
      echo "Tarring Build Directory"
      tar -C "$BUILD_DIR" --exclude=./requirements.txt -cf - . \
        | xz > "${SCRIPT_DIR}/axiom.tar.xz"
      echo "Rendering Template"
  fi

  exec 1>&3 3>&- # restore stdout and close fd 3

  if [ -z "$DONT_PULL" ]; then
    render_template < Dockerfile.tmpl
  else
    # Use the existing Dockerfile if we do not pull the source
    # The existing Dockerfile is passed as stdin
    cat
  fi
}

function build {
  $(aws ecr get-login --region "$REGION" | sed -re 's/\-e \b.*\b //g')

  ( set -x; docker build "$@" -t "$IMAGE" . )
}

function build_debug {
  dockerfile_debug > Dockerfile.debug

  docker build "$@" -t "$DEBUG_IMAGE" -f "${SCRIPT_DIR}/Dockerfile.debug" .
}

function run_bash {
  docker run -it "$@" "$IMAGE" /bin/bash
}

function run {
  docker run --detach=true "$@" "$IMAGE"
}

function create_repo {
  if aws ecr describe-repositories --repository-name "$NAME" --region "$REGION" &> /dev/null
  then
    echo "Docker respository for $NAME already exists"
  else
    echo "Creating a docker respository for $NAME"
    aws ecr create-repository --repository-name "$NAME" --region "$REGION" > /dev/null
  fi
}

function push {
  $(aws ecr get-login --region "$REGION" | sed -re 's/\-e \b.*\b //g')

  create_repo

  docker push "$IMAGE"
}

function dockerfile_debug {
  if [ ! -f "${SCRIPT_DIR}/Dockerfile.debug.tmpl" ]; then
    cat "${SCRIPT_DIR}/Dockerfile"
  else
    render_template < "${SCRIPT_DIR}/Dockerfile.debug.tmpl" \
      | sed \
        -e "s|@BASE_IMAGE@|$IMAGE|g" \
        -e "s|@NAME@|${NAME}|g"
  fi
}

function render_template {
  sed \
    -e "s|@BUILD_DIR@|$BUILD_DIR|g" \
    -e "s|@ENV_NAME@|$ENV_NAME|g" \
    -e "s|@ENV@|$ENV|g" \
    -e "s|@REGION@|$REGION|g" \
    -e "s|@REGISTRY@|$REGISTRY|g" \
    -e "s|@REGISTRY_SLASH@|$REGISTRY_SLASH|g" \
    -e "s|@RELEASE@|$RELEASE|g" \
    -e "s|@SOURCE@|$SOURCE|g" \
    -e "s|@TIMESTAMP@|$TIMESTAMP|g" \
    -e "s|@VERSION@|$VERSION|g"
}

function get_values_from_dockerfile {
  if [ ! -e "${SCRIPT_DIR}/Dockerfile" ]; then
    echo 'The Dockerfile does not exist' >&2
    exit 1
  fi

  # i'm not a fan of eval, but this is convenient
  eval $(sed -nre 's/^LABEL\s+(.*)$/\1/p;' < Dockerfile)

  : ${NAME:?No NAME label defined in the Dockerfile}
  : ${VERSION:?No VERSION label defined in the Dockerfile}
  : ${RELEASE:=1}
}

function pull_code_from_s3 {
  declare remote_src_dir="s3://predikto-axiom-config/src/${ENV_NAME}"

  SOURCE="s3://predikto-axiom-config/src/${ENV_NAME}"

  (
    echo "Copying the sparky source from $remote_src_dir"

    # The region is hardcoded here since the push to s3 by jenkins is always
    # in us-east-1
    set -x; time aws s3 sync --delete --quiet \
      "$remote_src_dir" "$BUILD_DIR" --region us-east-1
  )
}

function pull_code_from_git {
  declare branch=$1; shift || :
  declare git_working_dir=$1; shift || :
  declare commit= dirty= git_status

  if [ -z "$branch" -a -z "$git_working_dir" ]; then
    echo 'Unable to use git to deploy since no branch or git working dir was given'  >&2
    exit 1
  fi

  if [ -n "$branch" ]; then
    git clone -b "$branch" --depth 5 \
      'git@bitbucket.org:predikto/axiom.git' \
      "$BUILD_DIR"

    SOURCE=$(
      git --git-dir="${BUILD_DIR}/.git" show-ref --heads --tags "$branch" \
        | head -n1 | awk '{print $1}'
      )
  else
    rsync -a "${git_working_dir}/" "${BUILD_DIR}/"

    # Create an orphaned commit for uncommitted changes.
    # 'commit' is empty if there are no uncommitted changes.

    git_status=$(git \
      --git-dir="${git_working_dir}/.git" \
      --work-tree="${git_working_dir}" \
       status -s -uno)

    if [ -n "$git_status" ]; then
        # Set the dirty flag if there are unstaged or uncommitted changes
        dirty='-dirty'
    fi

    # 'git stash create' (2.13.0) has a bug where that command only works
    # inside the work tree. The work around is to cd into the work tree.
    commit=$(cd "${git_working_dir}" && git stash create)

    echo -n "Using the current branch in ${git_working_dir}"

    if [ -n "$commit" ]; then
      SOURCE="${commit}${dirty}"
    else
      SOURCE=$(
        git --git-dir="${git_working_dir}/.git" rev-parse HEAD
        )
    fi

    echo " with commit ${SOURCE}"
  fi
}

function get_version {
  declare version_file="${1:?No version filename given}"

  VERSION=$(awk -F "['\"]" 'BEGIN {IGNORECASE=1} /version/ {print $2}' \
    "$version_file")

  if [ -z "$VERSION" ]; then
    echo "Unable to find the version of axiom in $version_file"
    exit 1
  fi
}

main "$@"
