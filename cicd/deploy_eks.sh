#!/usr/bin/env bash
#
#BEGIN:
#
# usage: deploy_spark.sh [options]
#
# Use this script to install helm on a kubernetes cluster
#
#
#
# Options:
#   [kubectl options]
#     --kubectl <path>      Path to alternate kubectl binary
#
#     --kubeconfig <path>   Path to kubeconfig
#                           Defaults to kubectl defaults
#
#     --context <context>   kubectl context
#                           defaults to currently set kubectl context
#
#     --cluster <cluster>   kubectl cluster
#                           defaults to default cluster for context
#END:
#
set -eu -o pipefail

function usage {
  sed -e '0,/^#\s*BEGIN:$/d; /^#\s*END:\s*$/,$d; s/^#$//g; s/^#\s/ /g' < $0
}

HELM_OPTS=''

TLS=0

INTERACTIVE=1

KUBECTL=$(which kubectl)
K_OPTS=''

RABBIT_HOST=${RABBIT_HOST:-''}
RABBIT_USERNAME=${RABBIT_USERNAME:-''}
RABBIT_PASSWORD=${RABBIT_PASSWORD:-''}

PG_HOST=${PG_HOST:-''}
PG_USERNAME=${PG_USERNAME:-''}
PG_PASSWORD=${PG_PASSWORD:-''}

CONFIG_YAML=${CONFIG_YAML:-''}
TAG=${TAG:-''}
RELEASE=${RELEASE:-''}
NS=${NS:-''}

while [[ $# -gt 0 ]]; do
    case $1 in

        --kubectl )         shift
                            KUBECTL=$1
                            ;;
        --kubeconfig )      shift
                            K_OPTS="${K_OPTS} --kubeconfig=${1}"
                            ;;
        --context )         shift
                            K_OPTS="${K_OPTS} --context=${1}"
                            ;;
        -n | --namespace )  shift
                            NS=${1}
                            ;;
        --rabbitmq_user )   shift
                            RABBIT_USERNAME=${1}
                                ;;
        --rabbitmq_pass )   shift
                            RABBIT_PASSWORD=${1}
                            ;;
        --rabbitmq_host )   shift
                            RABBIT_HOST=${1}
                            ;;
        --pg_user )         shift
                            PG_USERNAME=${1}
                            ;;
        --pg_pass )         shift
                            PG_PASSWORD=${1}
                            ;;
        --pg_host )         shift
                            PG_HOST=${1}
                            ;;
        --tag )             shift
                            TAG=${1}
                            ;;
        -h | --help )       usage
                            exit
                            ;;
        * )                 echo "unsupported option ${1} ${2}"
                            usage
                            exit 1
    esac
    shift
done


${KUBECTL} ${K_OPTS} -n ${NS} create secret generic rabbitmq \
 --from-literal=username=${RABBIT_USERNAME} \
 --from-literal=password=${RABBIT_PASSWORD} \
 --from-literal=host=${RABBIT_HOST} \
 --validate --dry-run -o yaml | ${KUBECTL} ${K_OPTS} apply -f -

${KUBECTL} ${K_OPTS} -n ${NS} create secret generic postgres \
 --from-literal=username=${PG_USERNAME} \
 --from-literal=password=${PG_PASSWORD} \
 --from-literal=host=${PG_HOST} \
 --validate --dry-run -o yaml | ${KUBECTL} ${K_OPTS} apply -f -

sed "s/{{ env }}/${NS}/g; s/{{ tag }}/${TAG}/g" \
    yamls/Deployment.yaml | ${KUBECTL} ${K_OPTS} create -n ${NS} --validate --dry-run -o yaml -f - | ${KUBECTL} ${K_OPTS} -n ${NS} apply -f -

${KUBECTL} ${K_OPTS} create -n ${NS} -f yamls/HPA.yaml --validate --dry-run -o yaml | ${KUBECTL} ${K_OPTS} -n ${NS} apply -f -