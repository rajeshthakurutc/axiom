Axiom Worker
============

author: Marlee Stevenson (marlee.stevenson@utc.com)

The axiom worker is instantiated by worker.py in the root directory of 
this repository, which routes tasks using runner.py in axiom worker. The
tasks are routed to the axiom service