import os
from setuptools import setup, find_packages
from codecs import open
from os import path
from distutils.command.register import register as register_orig
from distutils.command.upload import upload as upload_orig

VERSION = '0.2'

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# get the dependencies and installs
with open(path.join(here, 'requirements.txt'), encoding='utf-8') as f:
    all_reqs = f.read().split('\n')

install_requires = [x.strip() for x in all_reqs if 'git+' not in x]
dependency_links = [x.strip().replace('git+', '') for x in all_reqs if 'git+' in x]

class upload(upload_orig):
    def _get_rc_file(self):
        return os.path.join('.', '.pypirc')
        
setup(
    name='axiom',
    version=VERSION,
    description='The axiom worker is instantiated by worker.py in the root directory of this repository, which routes tasks using runner.py in axiom worker. The tasks are routed to the axiom service',
    long_description=long_description,
    url='https://bitbucket.org/predikto/axiom',
    license='Private',
    classifiers=[],
    keywords='',
    packages=find_packages(exclude=['docs', 'tests*']),
    include_package_data=True,
    install_requires=install_requires,
    dependency_links=dependency_links,
    cmdclass={
        'upload': upload,
    }
)
