pipeline {
    agent any
    stages {
        stage('Test') {
            agent {
                dockerfile {
                    filename 'Dockerfile'
                }
            }
            steps {
                // This will be changed to 'pipdeptree --warn fail' eventually.
                // Doing so will cause the build to fail when depency conflicts
                // are not resolved.
                sh '~/venv/bin/pipdeptree'

                // The '|| true' should be removed once unit tests are in a working
                // state. Right now I need this to pass for testing the pipeline.
                sh '~/venv/bin/python -m nose2'
            }
        }
        stage('Build & Deploy Container') {
            when {
                expression {
                    if (env.BRANCH_NAME == 'master') { // prod
                        try {
                            // This will raise an exception when the current commit isn't a tag
                            sh 'git describe --exact-match --tags ' + env.GIT_COMMIT
                            return true
                        } catch (exc) {
                            return false
                        }
                    } else if (env.BRANCH_NAME.startsWith('release/')) { // test
                        return true
                    } else if (env.BRANCH_NAME == 'develop') { // dev
                        return true
                    }
                    return false
                }
            }

            steps {
                script {
                    deploy_env = 'dev'
                    kube_cluster = 'dev'
                    if (env.BRANCH_NAME == 'master') {
                        deploy_env = 'prod'
                        kube_cluster = 'prod'
                    } else if (env.BRANCH_NAME.startsWith('release/')) {
                        // "test" is on prod kube cluster
                        deploy_env = 'test'
                        kube_cluster = 'prod'
                    }

                    withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'ecr_full_access', variable: 'AWS_ACCESS_KEY_ID'],
                                     [$class: 'AmazonWebServicesCredentialsBinding', credentialsId: "aws-creds-${deploy_env}", accessKeyVariable: 'SPARKY_AWS_ACCESS_KEY', secretKeyVariable: 'SPARKY_AWS_SECRET_KEY'],
                                     file(credentialsId: "jenkins-kube-config-${kube_cluster}", variable: 'K_CONFIG'),
                                     string(credentialsId: "rabbitmq-${deploy_env}.host", variable: 'RABBIT_HOST'),
                                     string(credentialsId: "rabbitmq-${deploy_env}.username", variable: 'RABBIT_USERNAME'),
                                     string(credentialsId: "rabbitmq-${deploy_env}.password", variable: 'RABBIT_PASSWORD'),
                                     string(credentialsId: "postgres-host-${deploy_env}", variable: 'PG_HOST'),
                                     usernamePassword(credentialsId: "postgres-${deploy_env}", usernameVariable: 'PG_USERNAME', passwordVariable: 'PG_PASSWORD')
                    ]) {
                        dir('cicd/docker') {
                            sh(script: """#!/bin/bash -l
                                set -e

                                ./image.sh build -e ${deploy_env} -r us-east-1 -n ${env.BUILD_ID} -d ../../
                                ./image.sh push -x -e ${deploy_env} -r us-east-1 -n ${env.BUILD_ID} -d ../../
                               """)
                            docker_tag = sh(script: "./image.sh show -x -e ${deploy_env} -r us-east-1 -n ${env.BUILD_ID} -d ../../ | xargs", returnStdout: true).trim().tokenize(":")[1]
                        }
                        dir('cicd') {
                            sh(script: """#!/bin/bash
                                echo "deploying to env ${deploy_env}"
                                ./deploy_eks.sh -n ${deploy_env} --kubeconfig ${K_CONFIG} --tag ${docker_tag}
                               """)
                        }
                    }
                }
            }
        }
        stage('Insert Configs & Docs') {
            when {   // Temporary shitty thing. Waiting on https://issues.jenkins-ci.org/browse/JENKINS-41187
                expression {
                    if (env.BRANCH_NAME == 'master') { // prod
                        try {
                            // This will raise an exception when the current commit isn't a tag
                            sh 'git describe --exact-match --tags ' + env.GIT_COMMIT
                            return true
                        } catch (exc) {
                            return false
                        }
                    } else if (env.BRANCH_NAME.startsWith('release/')) { // test
                        return true
                    }
                    return false
                }
            }
            steps {
                script {
                    sh 'virtualenv -p $(which python3) venv'
                    sh 'venv/bin/python -m pip install numpy'
                    sh 'venv/bin/python -m pip install --upgrade  --index-url=http://10.0.0.133:6543/pypi/ --trusted-host=10.0.0.133 -r requirements.txt'
                    if (env.BRANCH_NAME == 'master') { // prod
                        sh 'venv/bin/python ./insert_configs.py ENV=PROD'
                    } else {
                        sh 'venv/bin/python ./insert_configs.py ENV=TEST'
                    }
                }
                script {
                    //sh 'venv/bin/python -m pip install  --index-url=http://10.0.0.133:6543/pypi/ --trusted-host=10.0.0.133 -r requirements.txt'
                    sh 'venv/bin/python -m pip install --upgrade --no-binary ":all:" --index-url=http://10.0.0.133:6543/pypi/ --trusted-host=10.0.0.133 setuptools'
                    sh 'venv/bin/python -m pip install --upgrade --no-binary ":all:" --index-url=http://10.0.0.133:6543/pypi/ --trusted-host=10.0.0.133 sphinx'
                    sh 'venv/bin/python -m pip install --upgrade --no-binary ":all:" --index-url=http://10.0.0.133:6543/pypi/ --trusted-host=10.0.0.133 packaging'
                    sh 'venv/bin/python docs_preprocess.py'
                    dir('./docs') {
                        // clean the build dir and rebuild
                        sh 'rm -rf build/'
                        sh '../venv/bin/sphinx-build -b html source build/html/'
                        // sync everything
                        if (env.BRANCH_NAME == 'master') { // prod
                            withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'ecr_full_access', variable: 'AWS_ACCESS_KEY_ID']]) {
                                sh 'aws s3 sync --delete build/html/ s3://predikto-documents-test/axiom/prod'
                            }
                        } else {
                            withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'ecr_full_access', variable: 'AWS_ACCESS_KEY_ID']]) {
                                sh 'aws s3 sync --delete build/html/  s3://predikto-documents-test/axiom/test'
                            }
                        }
                    }
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}


