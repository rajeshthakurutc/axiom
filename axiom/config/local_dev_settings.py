import os

QUEUE_HOSTNAME = os.getenv('QUEUE_HOSTNAME', 'localhost')
QUEUE_USERNAME = os.getenv('QUEUE_USERNAME', 'guest')
QUEUE_PASSWORD = os.getenv('QUEUE_PASSWORD', "guest")
RPC_QUEUE = os.getenv('RPC_QUEUE', 'rpc_queue')
JOB_QUEUE = os.getenv('JOB_QUEUE', 'job_queue')
EUROPE_RPC_QUEUE = os.getenv('EUROPE_RPC_QUEUE', 'europe_rpc_queue')
AXIOM_QUEUE = os.getenv('AXIOM_QUEUE', 'axiom_queue')
EUROPE_AXIOM_QUEUE = os.getenv('EUROPE_AXIOM_QUEUE', 'europe_axiom_queue')

# Connection information for the executor
CONFIG_DATABASE = os.getenv('CONFIG_DATABASE', 'PREDIKTO_PLATFORM')
CONFIG_USERNAME = os.getenv('CONFIG_USERNAME', 'postgres')
CONFIG_PASSWORD = os.getenv('CONFIG_PASSWORD', 'admin')
CONFIG_HOSTNAME = os.getenv('CONFIG_HOSTNAME', 'localhost')

VAULT_URL = os.getenv('VAULT_URL', None)
VAULT_ROLE = os.getenv('VAULT_ROLE', 'testing')
