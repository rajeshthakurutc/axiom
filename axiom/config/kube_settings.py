import os

QUEUE_HOSTNAME = os.environ['QUEUE_HOSTNAME']
QUEUE_USERNAME = os.environ['QUEUE_USERNAME']
QUEUE_PASSWORD = os.environ['QUEUE_PASSWORD']

RPC_QUEUE = os.getenv('RPC_QUEUE', 'rpc_queue')
JOB_QUEUE = os.getenv('JOB_QUEUE', 'job_queue')
EUROPE_RPC_QUEUE = os.getenv('EUROPE_RPC_QUEUE', 'europe_rpc_queue')
AXIOM_QUEUE = os.getenv('AXIOM_QUEUE', 'axiom_queue')
EUROPE_AXIOM_QUEUE = os.getenv('EUROPE_AXIOM_QUEUE', 'europe_axiom_queue')

# Connection information for the executor
CONFIG_DATABASE = os.getenv('CONFIG_DATABASE', 'PREDIKTO_PLATFORM')
CONFIG_USERNAME = os.getenv('CONFIG_USERNAME', 'predikto')
CONFIG_PASSWORD = os.environ['CONFIG_PASSWORD']
CONFIG_HOSTNAME = os.environ['CONFIG_HOSTNAME']

VAULT_URL = os.getenv('VAULT_URL', None)
VAULT_ROLE = os.getenv('VAULT_ROLE', 'testing')
