"""
.. module:: Runner_Worker
    :platform: Unix, Linux, Windows
    :synopsis: Takes in a single service config and builds it, then runs it.

:copyright: (c) 2019 United Technologies

"""

from __future__ import print_function, division, absolute_import
import gc
from putils.Interfaces import CentralDBInterface as cdb
from putils.Parser import safe_parse
from axiom.services import blended_watchlist, batch_end2end


def build_task(task_type, task_config, central_db, worker_name):
    """
    This takes in a task type and config from the worker (send here via callback), along with a CDB to build up the
    actual service class (those are in the services directory). In a netflix-money world, each of these services would
    probably be their own repo, they are seperate things. For now, we put them all here because they use the same
    hardware, and dispatch them out here.

    :param task_type:
    :param task_config:
    :param central_db:
    :rtype Service:

    """

    # Parse out the args and instantiate the service based on the type passed.
    args = task_config
    if 'BLENDEDWATCHLIST' in task_type:
        service_model = blended_watchlist.BlendedWatchlist(central_db, worker_name, kwargs=args)
    elif 'BATCHEND2END' in task_type:
        service_model = batch_end2end.BatchEnd2End(central_db, worker_name, kwargs=args)
    else:
        raise Exception('UNKNOWN TASK TYPE: %s' % (task_type,))

    # if there are sub_tasks present, then add them using the base function add_sub_tasks.
    service_model.set_name(
        task_config['name'],
        service_name=task_config.get('service_name'),
        config_name=task_config.get('config_name'),
        customer_id=task_config.get('customer_id'),
        job_id=task_config.get('job_id')
    )

    return service_model


def run(config, worker_name, vault_url=None, vault_role=None):
    """
    Takes in a dict with a section for 1 task, source, target, and logger, then builds and runs the task.

    example config:

    [CENTRAL]
    username = sdfgsdfgs
    password = sdfasdf
    host = safrwef

    [TASK]
    task_type = LARRY
    param = asdfa
    param2 = asdfa
    name = nametest

    :param config:
    :return:

    """

    # parse out the task type, and the config params from the config.
    task_config = config['TASK']
    task_type = safe_parse(task_config, 'task_type', 'str')

    # separated out parts related to target and sources
    central_config = config['CENTRAL']

    # Set up the central database
    c_username = safe_parse(central_config, 'username', 'str')
    c_password = safe_parse(central_config, 'password', 'str')
    c_host = safe_parse(central_config, 'host', 'str')
    central_db = cdb(c_host, c_username, c_password, vault_url=vault_url, vault_role=vault_role)

    # build and run the task
    task = build_task(task_type, task_config, central_db, worker_name)
    central_db.write_internal_log('Running Task {task}'.format(task=task))

    try:
        task.run()
    finally:
        try:
            central_db.logout()
        except Exception as e:
            pass

        # this may not be really necessary, but we want to be extra sure that we close these connections to avoid
        # cascading failures.
        gc.collect()
