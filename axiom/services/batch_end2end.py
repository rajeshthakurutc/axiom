import sys
import traceback
import time
import loggy_mclogface
from io import StringIO
from contextlib import redirect_stderr, redirect_stdout
from putils.Services.AbstractService import BaseService
from libsnipe.rules.process import process_end2end
from libsnipe.utils import file_utils


@loggy_mclogface.add_logger("AXIOM", attributes=['customer_id', 'worker_name', 'service_name', 'task_name', 'job_name',
                                                 'job_id'])
class BatchEnd2End(BaseService):
    # The verbose configs object is the input contract for all task configs of this type. The various input parameters
    # that can be sent are here as keys, each one of these will be available in the class as an instance variable. The
    # parameters are all descripted, typed, have options (maybe), have defaults (maybe), and are either optional or
    # required. This object is used by insert_configs.py to populate the config_builder database in cdb, used for
    # linting configs, runtime checking, and populating the tables for the PCC config builder UIs.

    _verbose_configs = {
        'good_cohort': {
            'description': 'file path of good cohort json',
            'type': 'str',
            'options': None,
            'default': None,
            'optional': False
        },
        'bad_cohort': {
            'description': 'file path of bad cohort json',
            'type': 'str',
            'options': None,
            'default': None,
            'optional': False
        },
        'unknown_cohort': {
            'description': 'file path of unknown cohort json',
            'type': 'str',
            'options': None,
            'default': None,
            'optional': False
        },
        'bucket_name': {
            'description': 's3 bucket to read from/write to',
            'type': 'str',
            'options': None,
            'default': None,
            'optional': False
        },
        'access_key': {
            'description': 's3 access key',
            'type': 'str',
            'options': None,
            'default': None,
            'optional': False
        },
        'secret_key': {
            'description': 's3 secret key',
            'type': 'str',
            'options': None,
            'default': None,
            'optional': True
        },
        'file': {
            'description': 'full filepath/filename(s) of csv data (all filepath/filenames in one string separated by commas)',
            'type': 'str',
            'options': None,
            'default': None,
            'optional': False
        },
        'save_dir': {
            'description': 'full filepath for output directory',
            'type': 'str',
            'options': None,
            'default': None,
            'optional': False
        },
        'devicecol': {
            'description': 'column name that contains device id',
            'type': 'str',
            'options': None,
            'default': 'device_id',
            'optional': True
        },
        'datecol': {
            'description': 'column name that contains date',
            'type': 'str',
            'options': None,
            'default': 'time_stamp',
            'optional': True
        },
        'timeunit': {
            'description': 'time unit',
            'type': 'str',
            'options': None,
            'default': 'ns',
            'optional': True
        },
        'rulefile': {
            'description': 'a valid rulefile containing all rules to be executed in this batch',
            'type': 'str',
            'options': None,
            'default': None,
            'optional': False
        },
        'iters': {
            'description': 'number of iterations in the threshold optimization search',
            'type': 'int',
            'options': None,
            'default': 10,
            'optional': True
        },
        'favor_recall': {
            'description': 'if true, will prefer high recall models over high precision',
            'type': 'bool',
            'options': [True, False],
            'default': False,
            'optional': True
        }
    }

    # This is a sample configuration, which is used by docs_preprocess.py to populate an example in the sphinx docs.
    _sample_json = {}

    def __init__(self, central_db, worker_name, **kwargs):
        """
            :param central_db:
            :param worker_name:
            :param kwargs:
        """

        self.name = None
        self.worker_name = worker_name
        self.central_db = central_db
        self.job_id = None
        self.service_name = None
        self.config_name = None
        self.customer_id = None
        self.service_name = 'Batch End2End'
        self.set_instance_vars_from_kwargs(**kwargs)

    def __str__(self):
        """
        Returns a readable string for print(obj)
        """

        return 'Batch End2End'

    def run(self):
        """
        The function which contains all workflow for the axiom process. This is the entry point for the service
        itself, it's what is called by the worker once initialization is complete.

        :rtype None:
        """
        start_time = time.time()

        # we log out to the central logs that we are starting, w/ timestamp, we also update the nike log in the
        # cdb, which is that runtime table in PCC.
        self.logger.info('Beginning %s' % self.name)
        if self.central_db is not None:
            self.central_db.start_task(self.name, worker_name=self.worker_name, project_name=self.config_name)

        # pull json files from s3 and write to json object
        rules = file_utils.read_single_from_s3(self.bucket_name, self.rulefile, self.access_key,
                                                          self.secret_key)
        good_cohort_json = file_utils.read_single_from_s3(self.bucket_name, self.good_cohort, self.access_key,
                                                             self.secret_key)
        bad_cohort_json = file_utils.read_single_from_s3(self.bucket_name, self.bad_cohort, self.access_key,
                                                             self.secret_key)
        unknown_cohort_json = file_utils.read_single_from_s3(self.bucket_name, self.unknown_cohort, self.access_key,
                                                             self.secret_key)

        stdout_io = StringIO()
        stderr_io = StringIO()

        # run batch_end2end
        try:
            with redirect_stdout(stdout_io):
                with redirect_stderr(stderr_io):
                    for item in rules:
                        rule = item.get('rule')
                        params = item.get('params')
                        days = item.get('days')

                        fields = list(params.values())
                        usecols = [self.devicecol, self.datecol] + fields
                        csv_df_merged = file_utils.get_merged_file_s3(self.file, self.bucket_name, self.access_key,
                                                      self.secret_key, usecols, self.devicecol, self.datecol,
                                                      self.timeunit)

                        thresholds, threshold, max_f1, row, false_pos, true_pos, false_neg, true_neg, no_sigs_good, no_sigs_bad, \
                        watchlist, rank, output_fig = process_end2end(
                            good_cohort_json=good_cohort_json,
                            bad_cohort_json=bad_cohort_json,
                            unknown_cohort_json=unknown_cohort_json,
                            csv_df=csv_df_merged,
                            rule=rule,
                            params=params,
                            days=days,
                            show=False,
                            favor_recall=self.favor_recall,
                            devicecol=self.devicecol,
                            datecol=self.datecol,
                            timeunit=self.timeunit,
                            iters=self.iters
                        )

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_tb(exc_tb, file=stderr_io)
            stderr_io.write(str(type(e)))
        finally:
            self.logger(stdout_io.getvalue())
            if stderr_io.getvalue():
                self.logger(stderr_io.getvalue(), level='WARNING')

        if self.central_db is not None:
            self.central_db.finish_task(self.name, project_name=self.config_name)

        end_time = time.time()
        self.logger.info('Finished {} at timestamp: {}'.format(self.name, end_time))
        self.logger.info('Time elapsed: {}'.format(end_time - start_time))
        self.logger.info('Completed: batch_end2end')

        return True
