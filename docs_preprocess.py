import inspect
import json
import os
from rstcloth import RstCloth
from axiom import services as axiom_services


def build_service_directory():
    funcs = {}
    for name in axiom_services.__all__:
        try:
            funcs.update({name: axiom_services.__dict__[name]})
        except:
            print('Failed to parse out the task: %s' % (name,))
            pass

    # create an Rst object and put the page title on it
    node_info = RstCloth()
    node_info.title('Service Directory')
    node_info.newline()

    for task_name in sorted(list(funcs.keys())):
        if task_name in ['summarizer_plugins']:
            continue

        v = funcs.get(task_name)

        # make a new section for the
        node_info.newline()
        node_info.content('-----------')
        node_info.newline()

        node_info.h2(task_name.replace('_', ' ').title())
        node_info.newline()

        # pull out the description and throw that in the section
        task_description = v.__str__(None)
        task_example = v.__dict__.get('_sample_json', {})

        node_info.h3('Description')
        node_info.newline()
        node_info.content(" ".join(task_description.split()))
        node_info.newline()

        # try to find a longer description
        if os.path.isfile('./docs/source/services/%s.rst' % (task_name.lower(),)):
            with open('./docs/source/services/%s.rst' % (task_name.lower(),), 'r') as f:
                node_info._data.append(f.read())
                node_info.newline()

        node_info.h3('Configuration Options')
        node_info.newline()

        # pull out the task parameters and handle the null case
        task_parameters = [param for param in v.__dict__.get('_verbose_configs', {}).keys()]
        if not task_parameters:
            task_parameters.append('no_parameters')

        # now do the inserting of each parameter
        for parameters in task_parameters:
            if parameters != 'no_parameters':
                parameter_name = str(parameters).replace('**', '')
                parameter_description = str(
                    v.__dict__.get('_verbose_configs', {}).get(parameters, {}).get('description', None))
                parameter_type = v.__dict__.get('_verbose_configs', {}).get(parameters, {}).get('type', None)
                parameter_options = str(v.__dict__.get('_verbose_configs', {}).get(parameters, {}).get('options', None))
                parameter_default = str(v.__dict__.get('_verbose_configs', {}).get(parameters, {}).get('default', None))
                parameter_optional = v.__dict__.get('_verbose_configs', {}).get(parameters, {}).get('optional', True)
            else:
                parameter_name = 'no_parameters'
                parameter_description = ''
                parameter_type = None
                parameter_options = 'null'
                parameter_default = 'null'
                parameter_optional = True

            parameter_description = parameter_description.replace('\r', ' ')
            parameter_description = parameter_description.replace('\n', ' ')
            parameter_description = parameter_description.replace('\t', ' ')
            parameter_description = ' '.join(parameter_description.split())
            if parameter_optional:
                node_info.li('**%s**: (%s, optional deafult=%s) %s' % (
                    parameter_name, parameter_type, parameter_default, parameter_description), wrap=False)
            else:
                node_info.li('**%s**: (%s) %s' % (parameter_name, parameter_type, parameter_description), wrap=False)
            # node_info.newline()

        node_info.newline()

        node_info.h3('Example')
        node_info.newline()

        node_info.content('.. code-block:: javascript')
        node_info.newline()
        node_info.content(json.dumps(task_example, sort_keys=True, indent=4), indent=4)
        node_info.newline()
        node_info.newline()

        node_info.content('.. autoclass:: %s' % (inspect.getmodule(v).__name__ + '.' + str(v.__name__)), wrap=False)
        node_info.content('\t:members:')

    node_info.write('./docs/source/service_directory.rst')


if __name__ == '__main__':
    build_service_directory()
