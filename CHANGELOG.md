0.1.7
=====

 * upping psycopg2 version

0.1.6
=====

 * upping putils version

0.1.5
=====

 * upping libsnipe version

0.1.4
=====

 * worker runs blended_watchlist only, uses new lib-snipe logic

0.1.3
=====

 * lowercasing service name for postgresql db

0.1.2
=====

 * addition of missing requirements

0.1.1
=====

 * addition of necessary docs

0.1.0
=====

 * first axiom release
