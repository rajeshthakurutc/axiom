import sys
import datetime
import json
import os
from axiom import services as axiom_services
from putils.Drivers.PostgreSQLDriver import PostgreSQLDriver


def upsert_service(db_driver):
    count = 0

    funcs = {}
    for name in axiom_services.__all__:
        try:
            funcs.update({name: axiom_services.__dict__[name]})
        except:
            print(name)
            pass

    for k, v in funcs.items():
        count += 1
        service_name = k.lower()

        # pull out task descriptions
        service_description = v.__str__(None)

        # pull out the task example and coerce it to a string
        service_example = v.__dict__.get('_sample_json', {})
        if isinstance(service_example, dict):
            service_example = json.dumps(service_example, indent=4, sort_keys=True)

        # pull out the task parameters and handle the null case
        service_parameters = [param for param in v.__dict__.get('_verbose_configs', {}).keys()]
        if not service_parameters:
            service_parameters.append('no_parameters')

        # Upsert in the service
        db_driver.upsert(
            table='nodes',
            pk_fields=['node_name'],
            schema='config_builder',
            node_name=service_name,
            node_description=service_description,
            node_example=service_example,
            node_type='service',
            node_source='axiom',
            created_time=datetime.datetime.now()
        )

        parameter_id_listing = []
        for parameters in service_parameters:
            if "missing _verbose_configs" in service_parameters:
                parameter_name = "missing _verbose_configs"
                parameter_description = "missing _verbose_configs"
                parameter_type = None
                parameter_options = None
                parameter_default = None
                parameter_optional = None
            elif parameters == 'no_parameters':
                parameter_name = parameters
                parameter_description = None
                parameter_type = None
                parameter_options = None
                parameter_default = None
                parameter_optional = None
            else:
                parameter_name = str(parameters).replace('**', '')
                parameter_name = service_name + '.' + parameter_name
                parameter_description = v.__dict__.get('_verbose_configs', {}).get(parameters, {}).get('description',
                                                                                                       None)
                parameter_type = v.__dict__.get('_verbose_configs', {}).get(parameters, {}).get('type', None)
                parameter_options = json.dumps(
                    {'options': v.__dict__.get('_verbose_configs', {}).get(parameters, {}).get('options', None)})
                parameter_default = json.dumps(
                    {'default': v.__dict__.get('_verbose_configs', {}).get(parameters, {}).get('default', None)})
                parameter_optional = v.__dict__.get('_verbose_configs', {}).get(parameters, {}).get('optional', None)

            # upsert the parameter
            db_driver.upsert(
                table='params',
                pk_fields=['parameter_name'],
                schema='config_builder',
                parameter_name=parameter_name,
                parameter_description=parameter_description,
                parameter_type=parameter_type,
                parameter_options=parameter_options,
                parameter_optional=parameter_optional,
                parameter_default=parameter_default,
                created_time=datetime.datetime.now()
            )

            # get the ids and upsert the relationship
            node_id = db_driver.execute(
                'SELECT node_id from config_builder.nodes where node_name=%s',
                sql_tuple=(service_name,),
                fetch=True
            )[0][0]

            parameter_id = db_driver.execute(
                'SELECT parameter_id from config_builder.params where parameter_name=%s',
                sql_tuple=(parameter_name,),
                fetch=True
            )[0][0]

            db_driver.upsert(
                table='rel',
                pk_fields=['node_id', 'parameter_id'],
                schema='config_builder',
                node_id=node_id,
                parameter_id=parameter_id,
                created_time=datetime.datetime.now()
            )

            parameter_id_listing.append(parameter_id)

        # now delete any ids not currently attached to the service
        if node_id is not None:
            db_driver.execute('DELETE from config_builder.rel where node_id=%s and parameter_id not in (%s)' % (
                node_id,
                ','.join([str(x) for x in parameter_id_listing])
            ))


if __name__ == '__main__':
    args = sys.argv
    ENV = os.getenv('ENV', 'TEST').upper()

    if ENV in ['DEV', 'TEST', 'PROD']:
        from axiom.config.kube_settings import *
    else:
        from axiom.config.local_dev_settings import *

    db_driver = PostgreSQLDriver(
        CONFIG_USERNAME,
        CONFIG_PASSWORD,
        CONFIG_HOSTNAME,
        port=5432,
        database=CONFIG_DATABASE
    )

    # first delete anything from source nike from the table
    try:
        node_ids = [str(x[0]) for x in
                    db_driver.execute('select node_id from config_builder.nodes where node_source=\'axiom\';',
                                      fetch=True)]
    except Exception as e:
        node_ids = []

    try:
        param_ids = [str(x[0]) for x in db_driver.execute(
            'select parameter_id from config_builder.rel where node_id in (%s);' % (','.join(node_ids)), fetch=True)]
    except Exception as e:
        param_ids = []

    if len(param_ids) > 0:
        db_driver.execute('delete from config_builder.rel where parameter_id in (%s)' % (','.join(param_ids)))
        db_driver.execute('delete from config_builder.params where parameter_id in (%s)' % (','.join(param_ids)))
    if len(node_ids) > 0:
        db_driver.execute('delete from config_builder.nodes where node_id in (%s)' % (','.join(node_ids)))

    upsert_service(db_driver)

    # Update View
    db_driver.execute('REFRESH MATERIALIZED VIEW config_builder.node_parameter_information WITH DATA;')
